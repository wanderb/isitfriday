let day = new Date().getDay();
console.log('Javascript available, using client time');

const urlParams = new URLSearchParams(window.location.search);

let nts = (12 - day) % 7;

if (urlParams.has('nts')) {
  nts = urlParams.get('nts') % 7;
}

window.addEventListener('DOMContentLoaded', (event) => {
  console.log('DOM Ready');
  const answer = document.getElementById('answer')
  const ntsline = document.getElementById('nts')
  if (nts == 0) {
    answer.classList.add('yes');
    answer.classList.remove('no');
    ntsline.style.display = 'none';
    answer.innerHTML = 'Yes!'
  } else {
    answer.classList.add('no');
    answer.classList.remove('yes');
    answer.innerHTML = 'NO'
    ntsline.innerHTML = `Just ${nts} more night${(nts > 1) ? 's' : ''} of sleep to go.`
  }
  console.log('Weekday: ' + day)
  console.log('NTS: ' + nts)
});
