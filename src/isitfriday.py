from flask import (
  Blueprint,
  flash,
  Flask,
  g,
  render_template,
  request,
  session
)

import gunicorn.app.base
import os
import sys
import logging
import datetime
from six import iteritems

prometheus_multiproc_dir = os.environ.get("PROMETHEUS_MULTIPROC_DIR", '/tmp/flask-prometheus-metrics')
os.environ['PROMETHEUS_MULTIPROC_DIR'] = prometheus_multiproc_dir
if not os.path.isdir(prometheus_multiproc_dir):
  os.makedirs(prometheus_multiproc_dir)

from prometheus_flask_exporter.multiprocess import GunicornPrometheusMetrics

allowed_loglevels = [
  'debug',
  'info',
  'warning',
  'error',
  'critical'
]

def create_app():
  app=Flask(__name__)
  gunicorn_logger = logging.getLogger('gunicorn.error')
  app.logger.handlers = gunicorn_logger.handlers
  app.logger.setLevel(gunicorn_logger.level)
  app.logger.info('Started, waiting for requests')
  metrics = GunicornPrometheusMetrics(app)

  @app.route("/")
  def home():
    nights_to_sleep = (11 - datetime.date.today().weekday()) % 7
    override = request.args.get("nts")
    if override:
      nights_to_sleep = int(override) % 7
    return render_template("friday.html", nights_to_sleep=nights_to_sleep)

  metrics.register_default(
    metrics.counter(
      'by_path_counter', 'Request count by request paths',
      labels={
        'path': lambda: request.path,
        'method': lambda: request.method,
        'status': lambda r: r.status_code
      }
    )
  )

  return app

class GunicornApp(gunicorn.app.base.BaseApplication):
  def __init__(self, app, options):
    self.options = options or {}
    self.application = app
    super(GunicornApp, self).__init__()

  def load_config(self):
    config = dict([(key, value) for key, value in iteritems(self.options)
                   if key in self.cfg.settings and value is not None])
    for key, value in iteritems(config):
      self.cfg.set(key.lower(), value)


  def load(self):
    gunicorn_logger = logging.getLogger('gunicorn.error')
    self.application.logger.handlers = gunicorn_logger.handlers
    self.application.logger.setLevel(gunicorn_logger.level)
    return self.application

if __name__ == "__main__":
  loglevel = os.getenv('LOGLEVEL', 'INFO').lower()
  if loglevel  not in allowed_loglevels:
    loglevel = 'info'


  options = {
    'bind': '0.0.0.0:8080',
    'loglevel': loglevel,
    'workers': 1,
    'accesslog': '-',
    'when_ready': 
      lambda server: GunicornPrometheusMetrics.start_http_server_when_ready(9011),
    'child_exit':
      lambda server, worker: GunicornPrometheusMetrics.mark_process_dead_on_child_exit(worker.pid),
  }

  app = create_app()
  GA = GunicornApp(app, options)
  GA.run()
