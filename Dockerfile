FROM registry.access.redhat.com/ubi9/python-311 AS builder
USER root
WORKDIR /build
COPY src /build
RUN pip3 install -r requirements.txt
RUN pyinstaller -s -F isitfriday.py --hidden-import=gunicorn.glogging --hidden-import=gunicorn.workers.sync --add-data static:static --add-data templates:templates
RUN mkdir -p /target/hcs-company.com
RUN mkdir -m 1777 /target/tmp
RUN staticx /build/dist/isitfriday /target/hcs-company.com/isitfriday; chmod 755 /target/hcs-company.com/isitfriday


FROM scratch
WORKDIR /
USER root
COPY --from=builder /target /
CMD ["/hcs-company.com/isitfriday"]
EXPOSE 8080
USER 1001
