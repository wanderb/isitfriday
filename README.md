# Is it Friday?

## Table of Contents
<!-- vim-markdown-toc GitLab -->

* [What does this do?](#what-does-this-do)
* [But why?](#but-why)
* [Limitations](#limitations)
* [I don't want to wait!](#i-dont-want-to-wait)
* [What's that `deploy` directory?](#whats-that-deploy-directory)
* [Contributing](#contributing)

<!-- vim-markdown-toc -->

## What does this do?

This web app does thing, and on thing only, answer that all important question
that gets asked in (home-)offices around the world every single day: **„Is it
Friday?”**.

It answers the question with a simple yes or no, and it even shows you how many
more nights of sleep you need before it will be Friday.

## But why?

I needed a simple application to demo and test (Tekton) pipelines that use
Source-to-Image (S2I) builds. This application, with its small dependencies and
quick build times fits the bill perfectly, and it's nicer than a static webpage
test. In the meantime it has morphed into a testbed for python single-binary
image builds and User Workload Monitoring.

If you are looking for the older S2I based build you can still find that one in
the `demo` branch.

## Limitations

This app is not very sophisticated. It looks at the current server time to
figure out the current weekday, and works from there. If the client has
javascript enabled in their browser the output is then later changed to reflect
the current client date.

## I don't want to wait!

Add `?nts=0` to the end of the URL. Replace `0` with how many nights you wish
it would be until Friday.

## What's that `deploy` directory?

It's a directory with [kustomize](https://kustomize.io) manifests for deploying
this app on OpenShift. Simply run `oc apply -k deploy/base` if you want a basic
deploy, or `oc apply -k deploy/prometheus` if you want a `ServiceMonitor`
deployed as well.

## Contributing

I'll take contributions, just create a merge request for me.
